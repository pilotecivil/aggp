#!/usr/bin/env python
# -*- coding: utf-8 -*-

## Individu.py
## MC-PC-AC-EF
#
# convention : ma_fonction; ma_variable

import networkx as nx
import matplotlib.pyplot as plt
from random import *
import scipy.stats as stats
import numpy as np
import math
from scipy.stats import kstest as ks
import time
import pylab as P


class Individu:

	def __init__(self,name, nb=0, rate_edge=0.1, ref=0, name_file=""):
		self.name=name
		self.sequence=[]
		self.nb_nodes=nb
		self.fitness=0
		self.graph=nx.Graph()
		self.reference=ref # si c'est une référence (E.Coli)
		self.rate_edge=rate_edge
		

		if self.reference:
			self.generate_sequence_from_file(name_file)
		else:
			self.generate_sequence()


	# affichage joli de la sequence (sous forme de matrice...) print object
	def __repr__(self):
		string=''
		esp=''
		for x in self.sequence:
			string+=str(esp)+str(x)+'\n'
			esp+='   '
		return string
		
	# initialise la séquence de l'individu aléatoirement
	def generate_sequence(self):
		for i in range(self.nb_nodes):
			self.sequence.append([np.random.binomial(1,self.rate_edge) for x in range(self.nb_nodes-i)])
		self.update_graph()

	#initialise la séquence de E.Coli
	def generate_sequence_from_file(self, name_file):
		f=open(str(name_file), 'r')
		self.nb_nodes=int(f.readline())

		for i in range(self.nb_nodes):
			self.sequence.append([randint(0,1) for x in range(self.nb_nodes-i)])
		
		self.graph.add_nodes_from(range(self.nb_nodes))

		for line in f:
			a,b=line.split(" ")
			a=int(a)
			b=int(b[:-1])
			self.graph.add_edge(a, b)

		for a,b in self.graph.edges():
			self.sequence[a][b-a]=1
	

	# met à jour le graph après mutation
	def update_graph(self):
		# put 0 to the diag
		for l in self.sequence:
			l[0]=0
		
		self.graph=nx.Graph()
		self.graph.add_nodes_from(range(self.nb_nodes))
		
		for i in range(self.nb_nodes): # parcourt les listes
			for j in range(self.nb_nodes-i): # parcourt l'intérieur des listes
				if self.sequence[i][j]!=0:
					self.graph.add_edge(i, j+i)
		
	def cytoscape(self, name) : 
		edges=self.graph.edges()
		nodes=self.graph.nodes()#		
		nodes_file=open(str(name)+".nodes", 'w')
		for i in range(len(nodes)) : 
			nodes_file.write(str(nodes[i]+1) +'\t' + str(nodes[i]+1) + '\n' )
			# num ID = txt a afficher a la representation
		nodes_file.close()
		
		edges_file=open(name+'.edges', 'w')
		for i in range(len(edges)) : 
			edges_file.write(str(edges[i][0]+1) +'\t'+ 'pp' +'\t' + str(edges[i][1]+1) + '\n' )
			# a voir s'il faut la 3e colonne avec le type d'interaction 
		edges_file.close()		

		# G = self.graph
		# nx.write_gml(G, name+'.gml')	
		# fait muter la séquence


	def generate_mutation(self, mutation_rate_01,mutation_rate_10):
		for i in range(len(self.sequence)):
			for j in range(len(self.sequence[i])):
				r=random()
				
				if self.sequence[i][j]==0 and r<mutation_rate_01:
					self.sequence[i][j]=1
				elif self.sequence[i][j]==1 and  r<mutation_rate_10:
					self.sequence[i][j]=0
		self.update_graph()
#		print "Nb arêtes : ", len(self.graph.edges())


	# calcule la fitness de l'individu
	def calc_fitness(self, list_param, nbPas=0, name_file="", write_ok=False):
		score=0
		names=['power law  ', 'clique     ', 'subgraph   ', 'small world']
		list_fct = [lambda : self.fitness_power_law(),lambda : self.fitness_cluster(),lambda : self.fitness_subgraph(), \
					lambda : self.fitness_small_world_other()]

		if write_ok: # on ecrit les resultats des fitness
			f=open(str(name_file)+'fit', 'a')
			f.write(str(nbPas)+'\t')

		for i in range(len(list_fct)):

			if list_param[i]!=0:
				score_func=list_fct[i]()
				score_inter=list_param[i] * score_func
#				print 'score',names[i],' : ', score_func
				score+= score_inter

				if write_ok:
					f.write(str(score_inter)+'\t')
			
		if write_ok:
			f.write('\n')
			f.close()
#		print 'score cluster : ', self.fitness_cluster()
		self.fitness=score


	# calcul de la propriété de loi de puissance, méthode continue
	def fitness_power_law(self):
		degrees=[]
		for i in self.graph.nodes() :
			degrees.append(self.graph.degree(i))
		compt={} # dictionnaire de comptage
		
		for i in range(self.nb_nodes) : 
			x=float(degrees.count(i)) # compte la presence de chaque degre
			if self.nb_nodes != 0 : # On evite de diviser par zero
				if x!=0.0:
					if i == 0 :
						compt[i] = math.log(x) 
						# compt[i] = math.log(x/self.nb_nodes) 
					else : 
						compt[math.log(i)] = math.log(x)         
						# compt[math.log(i)] = math.log(x/self.nb_nodes)
		
		# values without log :
		f=open('fitness_power_law','w')
		for i in range(len(compt.values())) : 
			f.write(str(compt.keys()[i])+'\t'+str(compt.values()[i])+'\n')
		f.close()

		# regress=[slope, intercept, r_value, p_value, std_err]
		regress=stats.linregress(compt.keys(),compt.values())
		r_squared = regress[2]**2
		if regress[0] >=0 :    
			score = 0   
		else :    
			score = r_squared

		# print 'R',score
		return score

	def fitness_cluster(self):
		
		coeff_moy = nx.average_clustering(self.graph)

		#degrees=[]
		# for i in self.graph.nodes() :
			# degrees.append(self.graph.degree(i))

		clustering = []  
		
		for i in range(self.nb_nodes):		
			clustering.append( nx.clustering(self.graph,i)  )
		

			
		variance=[]
		for i in range(len(clustering)) : 
			variance.append((clustering[i]-coeff_moy)**2)
		var=sum(variance)
		
		# Plus la variance est elevee, plus le coeff varie => mauvais score	
		
		if var < 0.001 :
			# print '->', 1/0.001
			return 1/0.001
			
		else :
			# print '->', 1/var
			return float(1/var)
		
				
	# calcul de la propriété de clique, méthode continue
	def fitness_clique(self):
		score=0
		degrees=[]
		for i in self.graph.nodes() :
			degrees.append(self.graph.degree(i))

		clustering = []  ## entrée sous la forme (C(k),k) 
		
		for i in range(self.nb_nodes):		
			clustering.append((degrees[i],  nx.clustering(self.graph,i)))
		
		# sortie graphique 	
		coordinates = zip(*clustering)	
		# plt.scatter(*coordinates)
		# plt.show()

		regress=stats.linregress(coordinates[0],coordinates[1])
		slope = regress[0]
		intercept = regress[1]
		std_error = regress[4]
		if slope>-0.01 and slope<0.01 :
			score = 100*regress[2]**2
			return score
						
		return score


	# calcul de la propriété subgraph
	def fitness_subgraph(self):
		if nx.is_connected(self.graph):
			return 1
		return 0

	# calcul de la propriété de petit monde
	def fitness_small_world_other(self):
		list_subgraph=nx.connected_component_subgraphs(self.graph)
		cpt=0
		sum_path=0
		for g in list_subgraph:
			if len(g.nodes())!=1:
				sum_path+=nx.average_shortest_path_length(g)
				cpt+=1
		average_path=sum_path/float(cpt)

		diff=abs(math.log(self.nb_nodes)-average_path)
		return 1/diff					




		
if __name__ == '__main__':

#	seed(1)
#	ordre parametres : power_law(),clique(),subgraph(), small_world()
	list_param=[1000,15,1,100]

## avec ces paramètres, fitness coli = 100.955752895

##########
########## test coli

	coli=Individu(1, 0,1,name_file="coli_genome.nde")
	print coli.nb_nodes
	coli.calc_fitness(list_param)
	print 'fitness coli :', coli.fitness
	print len(coli.graph.edges())

##########
########## test coli
	indiv=Individu(1, 40)
	indiv.calc_fitness(list_param)
	print 'fitness indiv :', indiv.fitness
#	print len(indiv.graph.edges())/float(indiv.nb_nodes)
#	print len(coli.graph.edges())/float(coli.nb_nodes)
	indiv.cytoscape('g')

