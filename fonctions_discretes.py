# calcul de la propriété de loi de puissance
	
#		GNUPLOT : 		
#		plot 'fitness_power_law' with lines
#		set logscale
#		plot 'fitness_power_law' with lines


	def fitness_power_law(self):
		degrees=[]
		for i in self.graph.nodes() :
			degrees.append(self.graph.degree(i))
		compt={} # dictionnaire de comptage
		
		for i in range(self.nb_nodes) : 
			x=float(degrees.count(i))
			if x!=0.0:
				compt[math.log(i)] = math.log(x/self.nb_nodes)
		
		# values without log :
		f=open('fitness_power_law','w')
		for i in range(len(compt.values())) : 
			f.write(str(compt.keys()[i])+'\t'+str(compt.values()[i])+'\n')
		f.close()

		# regress=[slope, intercept, r_value, p_value, std_err]
		regress=stats.linregress(compt.keys(),compt.values())
		r_squared = regress[2]**2
		
		r_squared_ref=0.45
		
		############## >> Methode DISCRETE : 

		
		if r_squared< r_squared_ref-0.4 : 
			score = 0
		elif r_squared< r_squared_ref-0.35 :
			score = 1
		elif r_squared< r_squared_ref-0.3 :
			score = 2
		elif r_squared< r_squared_ref-0.2 :
			score = 3			
		elif r_squared< r_squared_ref-0.1 :
			score = 4
		else :
			score = 5				

		return score


	# calcul de la propriété de clique
	def fitness_clique(self):
		score=0
		degrees=[]
		for i in self.graph.nodes() :
			degrees.append(self.graph.degree(i))

		clustering = []  ## entrée sous la forme (C(k),k) 
		for i in range(self.nb_nodes):		
			clustering.append((degrees[i],  nx.clustering(self.graph,i)))
		
		# sortie graphique 	
		coordinates = zip(*clustering)	
#		plt.scatter(*coordinates)
#		plt.show()

		regress=stats.linregress(coordinates[0],coordinates[1])
		slope = regress[0]
		intercept = regress[1]
		std_error = regress[4]
		
		############## >> Methode CONTINUE : 
		
		score=math.log(1/abs(slope))
		
		return score
		
		
		############## >> Methode DISCRETE : 

		# pour chacun des parametres : a +- qt(0.975,ddl)* SE
		# degree of freedom : 
		df = len(clustering)-2

		# Intervalle de confiance : 
		# qt=[IC_1%,IC_2.5%,IC_5%,IC_10%]
		qt = [stats.t.ppf(1-(0.01/2),df), stats.t.ppf(1-(0.025/2),df),stats.t.ppf(1-(0.05/2),df), stats.t.ppf(1-(0.1/2),df)]
		inf=[]
		sup=[]
		for i in range(4) : 
			inf.append(min(slope - qt[i]*std_error, slope + qt[i]*std_error))
			sup.append(max(slope - qt[i]*std_error, slope + qt[i]*std_error))
		
#		print inf, sup

		if inf[0]<0 and 0<sup[0] : 
			score = 4
		elif inf[1]<0 and 0<sup[1] :  
			score = 3
		elif inf[2]<0 and 0<sup[2] :  
			score = 2
		elif inf[3]<0 and 0<sup[3] :  
			score = 1
		else :
			score = 0
			
		
		return score


# test optimisation
	def fitness_small_world_test(self):
		score=0
		liste_path=[]
		for i in range(self.nb_nodes): # parcourt les listes
			liste_path.append(([0]*(self.nb_nodes-i)))
			for j in range(self.nb_nodes-i): # parcourt l'intérieur des listes			
				if i!=j:
					paths = list(nx.all_simple_paths(self.graph,source=self.graph.nodes()[i],target=self.graph.nodes()[j]))
					
					ni = len(paths)
					moyenne=0
					
					if ni!=0:
						somme=sum([len(paths[k])-1 for k in range(ni)])
						moyenne=float(somme)/float(ni)

					liste_path[i][j]=moyenne

		mean_path=[]
		for i in range(len(liste_path)):
			mean_path.append(sum(liste_path[i])/float(len(liste_path[i])))

		super_mean= float(sum(mean_path))/float((len(mean_path)))
		
		# On calcule la variance et l'écart type des distances moyennes des noeuds.
		variance=sum([(mean_path[k] - super_mean)**2 for k in range(len(mean_path))])
	
		sd_path=variance**0.5
		
		lnN = math.log(self.nb_nodes)  

		# On test si la distribution suit une loi normale centrée sur lnN
		mean_path_normed = [(mean_path[k]-lnN)/sd_path for k in range(len(mean_path))]

		test_norm = ks(mean_path_normed,'norm')
		print test_norm

		pvalue=test_norm[1]

		return score

	# calcul de la propriété de petit monde
	def fitness_small_world(self):
	
		score=0
		n = len(self.graph.nodes())
		# Les moyennes vers les autres noeuds
		li = np.zeros((n,n))
		
		# On calcule pour chaque noeud la distance vers les autres
		for i in range(len(li[0])):
			print i,"i"
			for j in range(len(li[1])):
				print j,"j"
			
				if self.graph.nodes()[i] != self.graph.nodes()[j]:
					print "entrée dans la condition"
					paths = nx.all_simple_paths(self.graph,source=self.graph.nodes()[i],target=self.graph.nodes()[j])
					l = list(paths)
					# Nbre de chemins
					
 
					ni = len(l)
					moyenne=0
					
					if ni!=0:
						somme = 0
						for k in range(ni):
#						print k,"k"
							somme += len(l[k])-1
						
						moyenne = float(somme)/float(ni)
					li[i,j] = moyenne
					
					#ecart = 0
					#for k in range(ni):
					#	ecart =  ecart + (len(l[k])-1 - moyenne)*(len(l[k])-1 - moyenne)
					#variance = ecart*1.0/n
					#si[i,j] = variance
		print "sortie de la boucle de small world"
		# On calcule la distance moyenne pour chaque noeud vers tous les autres
		mean_path=np.zeros((n,1))
		for i in range(len(li)):
			mean_path[i] = sum(li[i])/float(len(li[i])-1)
		super_mean= sum(mean_path)/(len(mean_path))
		
		# On calcule la variance et l'écart type des distances moyennes des noeuds.
		variance=0
		for k in range(len(mean_path)):
			variance += (mean_path[k] - super_mean)**2
#		
		sd_path=variance**0.5
		
		lnN = math.log(self.nb_nodes)  # Correspond à la moyenne théorique
		print lnN	
		# On test si la distribution suit une loi normal centré sur lnN
		mean_path_normed = (mean_path-lnN)/sd_path
		print mean_path	
		print mean_path_normed
		test_norm = ks(mean_path_normed,'norm')
		pvalue=test_norm[1]
		print super_mean,lnN,pvalue
		
		
#		print "les Li-------"
#		print li
#		print "Les si --------"
#		print si
	
	
		return score


