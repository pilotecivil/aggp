Pour lancer le programme :
	> python Population.py


En sortie :
- DOSSIER Graph :      
		graphes pour differents pas de temps sauves au format .png
- DOSSIER Cytoscape :  
		fichier graphe.nodes et graphe.edges pour une représentation sous cytoscape 
- DOSSIER Simulation : 
		ficher texte à 3 colonnes (pas de temps | fiteness_des_10_meilleurs | fitness_moyenne ) 
        il permet de tracer les courbes de fitness sous gnuplot


Pour modifier les paramètres de simulation :
	editer le ficher Population.py à la ligne 157


ANNEXE : 
- Schéma UML
- genome de reference E. coli
- fichier contenant le code des fonctions de fitness en discret 
