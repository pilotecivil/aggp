#!/usr/bin/env python
# -*- coding: utf-8 -*-

## Population.py
# nécessite Individu.py
## MC-PC-AC-EF
#
# convention : ma_fonction; ma_variable

from Individu import *
from operator import *
import random
import matplotlib.pyplot as plt
import copy
import pylab as P
import os

class Population:

	def __init__(self, nb_people, m_rate01,m_rate10, list_p, nb_nodes, rate_edge, indiv_ref=0, name_file=""):
		self.nb_people=nb_people
		self.ten_best_fitness=[]
		self.mutation_rate_01=m_rate01	
		self.mutation_rate_10=m_rate10
		self.parameter_fitness=list_p
		self.fitness_list=[]
		self.rate_edge=rate_edge
		self.compare_to_ref=indiv_ref

		self.list_people=[Individu(x, nb_nodes, self.rate_edge) for x in range(self.nb_people)]
		self.list_people_temp=[]
		
		if self.compare_to_ref:
			self.indiv_reference=Individu(0,1,name_file)
	
	
	# calcule la fitness de la population
	def calc_fitness(self, nbPas, name_file):
		sum_total=0
		i=0
		for x in self.list_people:
			if i==11:
				x.calc_fitness(self.parameter_fitness, nbPas, name_file, True)
			else:
				x.calc_fitness(self.parameter_fitness)
			i+=1

			sum_total+=x.fitness

		self.fitness_list.append(float(sum_total)/float(self.nb_people))

	# met à jour la liste des 10 meilleurs individus : CEUX QU'ON NE MUTE PAS !!!!!!!!!!!!!!
	def find_best_fitness(self):
		self.ten_best_fitness=[]
		list_t=[(i, self.list_people[i].fitness) for i in range(self.nb_people)]	
		list_t.sort(key=itemgetter(1), reverse=True)		
		
		for x in range(10):
			self.ten_best_fitness.append(copy.copy(self.list_people[list_t[x][0]]))

		moy=sum([g.fitness for g in self.ten_best_fitness])/10.0
		# print "m	 : ",moy
		# print len(self.ten_best_fitness[9].graph.edges())
		return moy

	# creer la population temporaire : CEUX QU'ON MUTE !!!!!!!!!!!!!!!!!!!!!!!
	def pop_temp(self):		
		# liste et tri des individus par leurs fitness decroissante
		l_indiv=zip(range(self.nb_people), [j.fitness for j in self.list_people])
		l_indiv.sort(key=itemgetter(1), reverse=True)		

		# création du sac : indiv i en prop i
		l_bag=[]
		for i in range(len(l_indiv)):	
			l_bag.extend([l_indiv[i][0] for j in range(i+1)]) # FIX IT ! Bug i+1 nécessaire !			

		# On sélectionne les indices : nb_pop -10 (correspondant aux 10_best)
		# tirage sans remise du sac
		l_indice=random.sample(l_bag, (self.nb_people-10))
		for i in l_indice:
			indiv=self.list_people[i]
			self.list_people_temp.append(copy.copy(indiv))



	def draw_best_graph(self, iteration) : 
		self.find_best_fitness()
		nx.draw(self.ten_best_fitness[0].graph) # arbitrairement le premier de la liste
		# self.ten_best_fitness[0].cytoscape('Cytoscape/best_graphe_'+str(iteration))		
		P.savefig('Graph/graphe_'+str(iteration)+'.png')
		P.close()
		

	# met à jour la population avec la population temporaire
	def update_population(self):
		# on ajoute les non mutés
		self.list_people=copy.copy(self.ten_best_fitness)
		# on rajoute les mutés
		self.list_people.extend(copy.copy(self.list_people_temp))
		# on remet à zéro
		self.list_people_temp=[]
		self.ten_best_fitness=[]		

	# réalise la mutation de la population
	def apply_mutation(self):
		# on mute que ceux qui sont pas les meilleurs
		for x in self.list_people_temp:
			x.generate_mutation(self.mutation_rate_01,self.mutation_rate_10)

	# teste la population à la référence
	def test_reference(self):
	    if not self.compare_to_ref:
	        return False

		if self.fitness_list[-1] < 1.1*self.indiv_reference.fitness and \
		 self.fitness_list[-1] > 0.9*self.indiv_reference.fitness:
			return True

		return False	

	# réalise la simulation de l'évolution des réseaux
	def evolutional_mechanism(self, nb_pas, name_fic):
		similar_to_ref=False
		i=0

		f_write=open(str(name_fic), "w")	

		while i<nb_pas:

			self.calc_fitness(i ,str(name_fic))
			moy=self.find_best_fitness()
			self.pop_temp()
			self.apply_mutation()
			self.update_population()				

			print i, moy, self.fitness_list[-1]

			f_write.write(str(i)+'\t'+str(moy)+'\t'+str(self.fitness_list[-1])+'\n')			
			
			# draw the graph of one of the ten best fitness 
			# every modulo steps 
   
			# nb_graph_to_draw = 8
			# modulo = nb_pas/nb_graph_to_draw
			# if i%modulo ==0 : 
			if i<10 or i> nb_pas-10:				
				self.draw_best_graph(str(i))
				
			i+=1

		f_write.close()


if __name__ == '__main__':

#########################################################################################################
##      PARAMETRES DE SIMULATION ;
#########################################################################################################	
	m_rate01 = 0.03
	m_rate10 = 0.35
	rate_edge =0.15



	nb_noeud = 60
	nb_indiv = 100
	nb_generation=50

	list_param = [550,100,10,100]
	# names=['power law  ', 'clique     ', 'subgraph   ', 'small world']
	

#########################################################################################################
#########################################################################################################	

	list_param = [list_param]
	for i in range(len(list_param)):
		print i
		try:
<<<<<<< HEAD
			dossier = "Ant_Final"+str(i)
=======
			dossier = "Simulation"
			#dossier = "Ant_comparaisonFitness"+str(i)
>>>>>>> 20b9f5d7e1a14224a197d49600f16d91aa60f7a0
			os.mkdir(dossier)
			os.mkdir('Cytoscape')
			os.mkdir('Graph')
		except OSError:
			pass
<<<<<<< HEAD
#(self, nb_people, m_rate01,m_rate10, list_p, nb_nodes, rate_edge, indiv_ref=0, name_file=""):

		pop=Population(50, 0.15, 0.2,list_param[i], 150,0.15)

		NAME_SORTIE = dossier + "/final_"+str(list_param[i][0])+"_"+str(list_param[i][1])+"_"+str(list_param[i][2])+"_"+str(list_param[i][3])+"_"
		pop.evolutional_mechanism(200,NAME_SORTIE)
=======
		#(self, nb_people, m_rate01,m_rate10, list_p, nb_nodes, rate_edge, indiv_ref=0, name_file=""):
		pop=Population(nb_indiv,m_rate01,m_rate10,list_param[i], nb_noeud,rate_edge)
		NAME_SORTIE = dossier + "/simulation_"+str(list_param[i][0])+"_"+str(list_param[i][1])+"_"+str(list_param[i][2])+"_"+str(list_param[i][3])+"_" + str(nb_noeud) + "_" + str(nb_indiv)
		pop.evolutional_mechanism(nb_generation,NAME_SORTIE)

>>>>>>> 20b9f5d7e1a14224a197d49600f16d91aa60f7a0

